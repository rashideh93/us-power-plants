# For Web Script:

# Running in a local environment

## Prerequisites

To run the Script you need `JKD > 17.X`  and `TestNG > v7.6.x`.


## Running
Please wait until all files is loaded, then you can run:
```
Go to src/test/java/Tests.java
```
```
Run all test cases
```


# For APIs:

# Running in a local environment

## Prerequisites

To run the Script you need `Postman`  or `Newman`.


## Running
```
Import the collection https://www.postman.com/collections/111d0566d41070f7920b on Postman.
```
```
Excute all Test cases under each API.
```
```
Note: Please change the port from URL (from 3001 to 9000)
```
