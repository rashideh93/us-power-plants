import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Tests {

    WebDriver driver;

    @BeforeMethod
    public void testSetup() {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://localhost:3000/");
    }

    @Test
    public void checkIfTheMapIsDisplaying() {

        PageObjectModel homeScreen = new PageObjectModel(driver);

        Assert.assertTrue(homeScreen.map.isDisplayed());
    }

    @Test
    public void checkIfByDefault200PowerPlantsAreDisplaying() {

        PageObjectModel homeScreen = new PageObjectModel(driver);

        Assert.assertTrue(homeScreen.numberOfPowerPlantsSelected.getAttribute("value").equalsIgnoreCase("200"));
    }

    @Test
    public void checkIfTheSliderIsFetchingTheCorrectNumberOfPlants() {

        PageObjectModel homeScreen = new PageObjectModel(driver);

        String numberOfPowerPlantsSelected = homeScreen.numberOfPowerPlantsSelected.getAttribute("value");
        String[] numberOfPowerPlantsDisplayedAsText = homeScreen.numberOfPowerPlantsDisplayedAsText.getText().split(" ");
        Assert.assertTrue(numberOfPowerPlantsSelected.equalsIgnoreCase(numberOfPowerPlantsDisplayedAsText[0]));
    }

    @Test
    public void checkIfTheSameNumberOfPlantsAreDisplayedOnTheMap() {

        PageObjectModel homeScreen = new PageObjectModel(driver);

        int iCount = 0;
        iCount = driver.findElements(By.xpath("//img[@src='https://unpkg.com/leaflet@1.7.1/dist/images/marker-icon-2x.png']")).size();
        String numberOfPowerPlantsSelected = homeScreen.numberOfPowerPlantsSelected.getAttribute("value");
        Assert.assertTrue(numberOfPowerPlantsSelected.equalsIgnoreCase(String.valueOf(iCount)));
    }

    @Test
    public void checkReturnedListOfPlantsAreBelongingToTheSameState() throws InterruptedException {

        PageObjectModel homeScreen = new PageObjectModel(driver);

        Select stateDropDown = new Select(driver.findElement(By.xpath("//select")));
        stateDropDown.selectByValue("MA");
        Thread.sleep(3000);
        String[] numberOfPowerPlantsDisplayedAsText = homeScreen.numberOfPowerPlantsDisplayedAsText.getText().split(" ");
        Assert.assertTrue(numberOfPowerPlantsDisplayedAsText[0].equalsIgnoreCase(String.valueOf(579)));
    }

    @AfterMethod
    public void testTearDown() {

        driver.quit();
    }
}