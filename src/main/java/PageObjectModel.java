import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageObjectModel {

    WebDriver driver;

    public PageObjectModel(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='leaflet-container leaflet-touch leaflet-retina leaflet-fade-anim leaflet-grab leaflet-touch-drag leaflet-touch-zoom']")
    public WebElement map;

    @FindBy(xpath = "//input[@type='range']")
    public WebElement numberOfPowerPlantsSelected;

    @FindBy(xpath = "(//div/div/div/h3)[1]")
    public WebElement numberOfPowerPlantsDisplayedAsText;

    @FindBy(xpath = "//img[@src='https://unpkg.com/leaflet@1.7.1/dist/images/marker-icon-2x.png']")
    public WebElement mapsDisplayedOnMaps;

}
